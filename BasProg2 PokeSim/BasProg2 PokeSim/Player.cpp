#include "Player.h"
#include "Pokemon.h"
#include <iostream>
#include<time.h>
using namespace std;

int randDamage = rand() % 20 + 10;
int randxP = rand() % 20 + 1;
int randlvl = rand() % 10 + 1;
int enc = rand() % 15 + 1;
int cap = rand() % 10 + 1;
Pokemons* wild = new Pokemons(100, 0, randDamage, 0, 40, randlvl);

Player::Player()
{
	this->name = "";
}

Player::Player(string name)
{
	this->name = name;
}

void Player::introduction(string name)
{    	cout << "Hello I am Professor Oak, and I welcome you to the wonderful land of Pokemon." << endl;
		system("pause");
		system("cls");
		cout << "May I know your name young Pokemon master?" << endl;
		cin >> this->name;
		system("pause");
		system("cls");
		cout << "Pleasant to meet you " << name << endl;
		cout << "I have three pokemon here with me, kindly choose one." << endl;
		system("pause");
		system("cls");
		cout << "[0] Bulbasaur        [1] Charmander        [2] Squirtle" << endl;
		cin >> starterChoice;
		system("pause");
		system("cls");
		cout << "You have chosen " << starters[starterChoice] << endl;
		system("pause");
		system("cls");
		
		wild->starter(NULL);
}

void Player::move(int x, int y, int pokemonInventorySize, int town)
{
	srand(time(NULL));
	int randDamage = rand() % 20 + 10;
	int randxP = rand() % 20 + 1;
	int randlvl = rand() % 10 + 1;
	int enc = rand() % 15 + 1;
	int cap = rand() % 10 + 1;
	int attackSuccess = rand() % 10 + 1;
	int encounterChance = rand() % 10 + 1;

	cout << "What would you like to do?" << endl;
	cout << "[1] Move      [2] Pokemon  " << endl;
	cout << "[3] Pokemon Center " << endl;

	for (;;)
	{
		cin >> activity;
		system("pause");
		if (activity == 1)
		{
			cout << "[w] up    [a] left    [s] down   [d] right " << endl;
			cin >> input;
			if (input == 'w')
			{
				y++;
				{
					if (y >= -2 && y <= 2 && x >= -2 && x <= 2)
					{
						cout << "Pallet town" << endl;
						town = 1;
					}
					else if (y > 2 && y <= 4 && x >= -2 && x <= 2)
					{
						cout << "Route 1" << endl;
						town = 0;
					}
					else if (y > 4 && x > 4)
					{
						cout << "Pewter City" << endl;
						town = 1;
					}
					cout << "(" << x << "," << y << ")" << endl;
				}
			}
			else if (input == 'a')
			{
				x--;
				if (y >= -2 && y <= 2 && x >= -2 && x <= 2)
				{
					cout << "Pallet town" << endl;
					town = 1;
				}
				else if (y > -2 && y <= 2 && x < -2 && x >= -4)
				{
					cout << "Route 2" << endl;
					town = 0;
				}
				else if (y < 4 && x < -4)
				{
					cout << "Mt. Moon" << endl;
					town = 0;
				}
				cout << "(" << x << "," << y << ")" << endl;
			}
			else if (input == 's')
			{
				y--;
				if (y >= -2 && y <= 2 && x >= -2 && x <= 2)
				{
					cout << "Pallet town" << endl;
					town = 1;
				}
				else if (y < -2 && y >= -4 && x >= -2 && x <= -2)
				{
					cout << "Route 24" << endl;
					town = 0;
				}
				else if (y < 4 && x < -4)
				{
					cout << "unknown" << endl;
					town = 0;
				}
				cout << "(" << x << "," << y << ")" << endl;
			}
			else if (input == 'd')
			{
				x++;
				if (y >= -2 && y <= 2 && x >= -2 && x <= 2)
				{
					cout << "Pallet town" << endl;
					town = 1;
				}
				else if (y > 2 && y <= 2 && x > 2 && x <= 4)
				{
					cout << "Viridian City" << endl;
					town = 1;
				}
				else if (y > 4 && x > 4)
				{
					cout << "unknown" << endl;
					town = 0;
				}
				cout << "(" << x << "," << y << ")" << endl;
			}

		}
		if (activity == 2)
		{

			wild->stats(100, 0, randDamage, randxP, 40, randlvl, 1, 5);
		}
		if (activity == 3)
		{
			cout << "welcome to the Pokemon Center.... /n would you like to heal your Pokemon?? y||n" << endl;
			cin >> input;
			if (input == 'y')
			{
				cout << "*Pokemon Center Music*" << endl;
				system("pause");
				cout << "." << endl;
				system("pause");
				cout << ".." << endl;
				system("pause");
				cout << "..." << endl;
				system("pause");
				cout << "Your Pokemons are healed... Thank you and see you again soon" << endl;
			}
			else if (input == 'n')
			{
				cout << "Thank you for visiting... We hope to see you again soon/n";
			}
		}
		system("pause");
		system("cls");
		if (town == 1)
		{
			cout << "What would you like to do?" << endl;
			cout << "[1] Move      [2] Pokemon  " << endl;
			cout << "[3] Pokemon Center " << endl;
		}
		else if (town == 0)
		{
			if (encounterChance <= 5)
			{
				wild->encounterPokemons(wild->enc, wild->randlvl, 100, wild->randDamage, 0, 40);

				cin >> activity;
				if (activity == 5)
				{
					if (attackSuccess <= 8)
					{
						wild->battle(100, randDamage, enc, 0, 40, 1);
					}
				}
				else if (activity == 6)
				{
					wild->capturePokemon(cap, 1, enc);
				}
				else if (activity == 7)
				{
					cout << "You ran away! " << endl;
				}
			}
			cout << "What would you like to do?" << endl;
			cout << "[1] Move      [2] Pokemon  " << endl;
		}
		
	}
}

